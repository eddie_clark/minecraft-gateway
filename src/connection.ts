import { Socket } from 'net'
import Config, { ProxyRoute } from './config'
import { BufferWrapper } from './protocol/utils/BufferWrapper'
import { Handshake } from './protocol/packets/serverBound/Handshake.packet'
import { PacketHandler } from './protocol/packets/PacketHandler'
import { State } from './protocol/packets/state'
import { StatusRequest } from './protocol/packets/serverBound/StatusRequest.packet'
import PlayerManager from './player/PlayerManager'
import { LoginStart } from './protocol/packets/serverBound/LoginStart.packet'
import { Packet } from './protocol/packets/Packet'
import { DisconnectBuilder } from './protocol/packets/clientBound/Disconnect.packet'
import { Status } from './server/models/Server'
import ServerApi from './server/ServerApi'

export class Connection {
  private playerSocket: Socket
  private serverSocket: Socket

  private packetHandler: PacketHandler = PacketHandler.instance

  private state = State.handshaking

  private playerUsername: string | undefined
  private proxyRoute: ProxyRoute | undefined

  constructor(playerSocket: Socket) {
    this.playerSocket = playerSocket
    this.serverSocket = new Socket()

    this.setupSocketsErrorHandlers()
    this.setupSocketsCloseHandlers()

    this.playerSocket.on('data', (data) => this.handlePlayerSocketData(data))
  }

  handlePlayerSocketData(playerSocketData: Buffer): void {

    try {
      const buff = new BufferWrapper(playerSocketData)

      const packet = this.packetHandler.readPacket(buff, this.state)

      if(!packet) {
        console.log('No packet (this is weird)');
        
        this.closeConnection();
        return;
      }

      // Handle the packet
      this.handlePacket(playerSocketData, packet);

      // Put the remaining data (packets) back in the buffer
      this.playerSocket.unshift(playerSocketData.slice(buff.packetOffset))


    } catch (e) {
      // TODO timeout after too many unshifting
      console.log("Unshifting");
      console.log(playerSocketData);

      //process.exit() //great for debug
      // Put all the data back in the buffer, this happen if the packet is not complete
      this.playerSocket.unshift(playerSocketData)
    }
  }

  handlePacket(playerSocketData: Buffer, packet: Packet): void {

    switch (this.state) {
      case State.handshaking:
        this.handshake(playerSocketData, packet)
        break
      case State.status:
        this.status(playerSocketData, packet)
        break
      case State.login:
        this.login(playerSocketData, packet)
        break
      default:
        break
    }
  }

  handshake(playerSocketData: Buffer, packet: Packet): void {

    if (!packet || !(packet instanceof Handshake)) {
      console.log("first packet wasn't handshake")
      this.closeConnection()
      return
    }

    this.state = packet.nextState

    // Setup server socket
    if (!this.connectServerSocket(packet.serverAddress)) {
      return
    }

    // Send handshake to server
    this.serverSocket.write(playerSocketData.slice(0, packet.totalLenght))
  }

  status(playerSocketData: Buffer, packet: Packet): void {

    if (!packet || !(packet instanceof StatusRequest)) {
      console.log("second packet wasn't status request")

      return
    }

    // Send status packet to server
    this.serverSocket.write(playerSocketData.slice(0, packet.totalLenght))

    // Send the rest of the buffer
    this.serverSocket.write(playerSocketData.slice(packet.totalLenght))

    // Bond them for server responses
    this.bindClientServer()
  }

  login(playerSocketData: Buffer, packet: Packet): void {

    if (packet && (packet instanceof LoginStart)) {
      // TODO create packet handler
      console.log(`User: ${packet.username}`);
      
      this.playerUsername = packet.username
      if (this.proxyRoute?.server?.id) {
        PlayerManager.handlePlayerConnection(
          this.proxyRoute.server.id,
          this.playerUsername
        )
      }
    }

    this.serverSocket.write(playerSocketData)
    this.bindClientServer()
  }

  connectServerSocket(serverAddress: string): boolean {
    // Récupération des informations de connexion du serveur en fonction de nom de domaine
    const hostname = serverAddress.split('.')[0]
    
    this.proxyRoute = Config.serveurList.get(hostname)
    if (!this.proxyRoute) {
      this.closeConnection(`Remote server is not available or doesn't exist`)
      return false
    }

    if (this.state == State.login && this.proxyRoute.server?.status == Status.OFF && this.proxyRoute.server?.autoStart) {
      console.log(`Starting server ${this.proxyRoute.server.id}`);
      
      ServerApi.startServer(this.proxyRoute.server.id)
      this.closeConnection(`Remote server is starting`)
      return false
    }

    if (this.proxyRoute.server?.status == Status.OFF) {
      this.closeConnection(`Remote server is offline`)
      return false
    }

    console.log(
      `Connecting ${serverAddress} => ${this.proxyRoute.internalHostname}:${this.proxyRoute.internalPort}`
    )

    this.serverSocket.connect(this.proxyRoute.internalPort, this.proxyRoute.internalHostname)
    return true
  }

  bindClientServer(): void {
    console.log('Binding Player <-> Server');
    
    this.playerSocket.removeAllListeners('data')
    this.playerSocket.pipe(this.serverSocket)
    this.serverSocket.pipe(this.playerSocket)
  }

  setupSocketsErrorHandlers() {
    this.playerSocket.on('error', (err) =>
      console.log(`Error on the client socket: ${err}`)
    )

    this.serverSocket.on('error', (err) =>
      console.log(`Error on the server socket: ${err}`)
    )
  }

  setupSocketsCloseHandlers() {
    this.playerSocket.on('close', () => {
      this.closeConnection()
    })

    this.serverSocket.on('close', () => {
      this.closeConnection()
    })
  }

  closeConnection(errorMessage?: string) {
    console.log('Closing Connection')

    if (errorMessage) {
      const disconnectPacket = DisconnectBuilder.error(errorMessage).toBuffer()
      this.playerSocket.write(disconnectPacket)
    }

    if(this.playerSocket.writable) {
      this.playerSocket.end()
    }

    if(this.serverSocket.writable) {
      this.serverSocket.end()
    }

    if (this.proxyRoute?.server?.id && this.playerUsername) {
      PlayerManager.handlePlayerDisconnection(
        this.proxyRoute.server.id,
        this.playerUsername
      )
    }
  }
}
