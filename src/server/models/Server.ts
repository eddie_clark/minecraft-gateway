// Represents the server resource from servers-service
export interface Server {
  id: number
  name: string
  hostname: string
  status: Status
  autoStart: boolean
}

export enum Status {
  ON = 'on',
  OFF = 'off'
}