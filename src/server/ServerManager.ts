import Config from '../config'
import ServerApi from './ServerApi'
import { Server } from './models/Server'

class ServerManager {
  async updateAvailableServers(): Promise<void> {
    const servers = await ServerApi.getServers()
    this.addServersToConfig(servers)
  }

  addServersToConfig(servers: Server[]): void {
    servers.forEach((server) => {
      Config.addRoutes([
        {
          externalHostname: server.hostname.split('.')[0],
          internalHostname: `server-${server.id}-minecraft-server.default.svc.cluster.local`,
          internalPort: Number(process.env.DEFAULT_SERVER_PORT) || 25565,
          server
        },
      ])
    })
  }
}

export default new ServerManager()